//
//  AccessibleViewProtocol.swift
//  MolotovApp
//
//  Created by Guillaume MIRAMBEAU on 31/01/2018.
//  Copyright © 2018 Molotov. All rights reserved.
//

import UIKit

public protocol AccessibleViewProtocol {
  func setAccessibility(with text: String?)
}

// ****************************************************************************
// MARK: - In App purchase handling

public extension AccessibleViewProtocol where Self: UIView {

  func setAccessibility(with text: String?) {
    guard let text = text else {
      isAccessibilityElement = false
      return
    }

    isAccessibilityElement = UIAccessibility.isVoiceOverRunning
    accessibilityLabel = text
  }
}
