//
//  ViewController.swift
//  Accessibility
//
//  Created by Guillaume Mirambeau on 14/06/2020.
//  Copyright © 2020 Guillaume Mirambeau. All rights reserved.
//

import UIKit

struct Section: Decodable {
	let name: String
	let items: [Item]
}

struct Item: Decodable {
	let name: String
	let accessibility: Accessibliliy?
}

struct Accessibliliy: Decodable {
	let buttonDescription: String
}

class ViewController: UIViewController {

	enum Constants {
		static let accessibleFileName = "DataAccessible"
		static let notAccessibleFileName = "DataNotAccessible"
		static let fileExtension = "json"
		static let defaultCellIdentifier = "TableViewCellIdentifier"
		static let castleCellIdentifier = "CastleTableViewCellIdentifier"
	}

	@IBOutlet var tableView: UITableView!
	var section: Section?

	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view.
		do {
			section = try Utils.object(from: Constants.notAccessibleFileName, fileExtension: Constants.fileExtension)
		} catch let error {
			print(error)
		}
	}

}

extension ViewController: UITableViewDataSource {
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return self.section?.items.count ?? 0
	}

	func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
		return self.section?.name
	}

	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		guard let cell = tableView.dequeueReusableCell(withIdentifier: Constants.castleCellIdentifier, for: indexPath) as? CastleTableViewCell else {
			return tableView.dequeueReusableCell(withIdentifier: Constants.defaultCellIdentifier, for: indexPath)
		}
		cell.configure(with: section?.items[indexPath.row])
		return cell
	}

}

extension ViewController: UITableViewDelegate {
	
}
