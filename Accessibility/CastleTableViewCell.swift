//
//  CastleTableViewCell.swift
//  Accessibility
//
//  Created by Guillaume Mirambeau on 14/06/2020.
//  Copyright © 2020 Guillaume Mirambeau. All rights reserved.
//

import UIKit

class CastleTableViewCell: UITableViewCell {
	@IBOutlet var addButton: UIButton!
	
	func configure(with item: Item?) {
		guard let item = item else { return }
		textLabel?.text = item.name
		
		// Accessibility
		guard let accessibility = item.accessibility else { return }
		addButton.setAccessibility(with: accessibility.buttonDescription)
	}
}
