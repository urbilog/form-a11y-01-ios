//
//  Utils.swift
//  Accessibility
//
//  Created by Guillaume Mirambeau on 14/06/2020.
//  Copyright © 2020 Guillaume Mirambeau. All rights reserved.
//

import Foundation
import UIKit

enum UtilsError: Error {
    case filePath
	case fileContent
}

class Utils {
	
	static func object<T : Decodable>(from fileName: String, fileExtension: String) throws -> T? {
		do {
			// Get file path
			guard let filepath = Bundle.main.path(forResource: fileName, ofType: fileExtension) else { throw UtilsError.filePath }
			// Get content as String
			let contents = try String(contentsOfFile: filepath)
			// Convert String content to Data
			guard let data = contents.data(using: .utf8) else { throw UtilsError.fileContent }
			// Parse the data through Decodable
			return try JSONDecoder().decode(T.self, from: data)
		} catch let error {
			print(error)
			return nil
		}
	}

}

extension UIButton: AccessibleViewProtocol {}
